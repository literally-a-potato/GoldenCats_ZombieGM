#if defined _goldc_zombies_included
  #endinput
#endif
#define _goldc_zombies_included

native bool GoldC_IsZombie(int client);
forward void GoldC_OnInfected(int client);

#if !defined REQUIRE_PLUGIN
public __pl_goldc_zombies_SetNTVOptional()
{
	MarkNativeAsOptional("GoldC_IsZombie");
}
#endif

public SharedPlugin __pl_goldc_zombies =
{
	name = "goldc_zombies",
	file = "goldc-zombies.smx",
#if defined REQUIRE_PLUGIN
	required = 1
#else
	required = 0
#endif
};
