#include <sourcemod>
#include <tf2>
#include <tf2items>
#include <tf2_stocks>
#include <clientprefs>
#include <sdkhooks>
#include <sourcemod-misc>
#include <goldc_zombies>
//#include <chat-processor>

#pragma semicolon 1
#pragma newdecls required


#include "gamemode/G_GamemodeInformation.sp"
#include "gamemode/G_GamemodeGlobals.sp"
#include "gamemode/G_GamemodePrimary.sp"
#include "gamemode/G_GamemodeSecondary.sp"
#include "gamemode/G_GamemodePatch.sp"
#include "gamemode/G_InteractionChat.sp"
#include "gamemode/G_InteractionClasslimits.sp"
#include "gamemode/G_InteractionHealthscale.sp"
#include "gamemode/G_InteractionItemPerks.sp"
#include "gamemode/G_InteractionPlayerSkins.sp"
#include "gamemode/G_InteractionRoundSounds.sp"
#include "gamemode/G_InteractionRoundTimer.sp"
#include "gamemode/G_InteractionWeaponChanges.sp"
#include "gamemode/G_InteractionZombieBase.sp"
#include "gamemode/G_GameserverStatistics.sp"