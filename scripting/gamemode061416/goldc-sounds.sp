//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>
#include <tf2_stocks>

//External Includes
#include <sourcemod-misc>

//Our Includes
#include <goldc_zombies>

//ConVars
ConVar convar_Sound_RoundEnd;
ConVar convar_Sound_RoundStart;
ConVar convar_Sound_HumanDies;
ConVar convar_Sound_LastManStanding;
ConVar convar_Sound_ZombiesWin;
ConVar convar_Sound_HumansWin;

//Globals

public Plugin myinfo =
{
	name = "[GoldC] Sounds",
	author = "Keith Warren (Drixevel)",
	description = "A basic sounds plugin for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public void OnPluginStart()
{
	LoadTranslations("common.phrases");
	
	convar_Sound_RoundEnd = CreateConVar("sm_goldc_sounds_roundend", "");
	convar_Sound_RoundStart = CreateConVar("sm_goldc_sounds_roundstart", "");
	convar_Sound_HumanDies = CreateConVar("sm_goldc_sounds_humandies", "");
	convar_Sound_LastManStanding = CreateConVar("sm_goldc_sounds_lastmanstanding", "");
	convar_Sound_ZombiesWin = CreateConVar("sm_goldc_sounds_zombieswin", "");
	convar_Sound_HumansWin = CreateConVar("sm_goldc_sounds_humanswin", "");
	
	HookEvent("teamplay_round_win", Event_OnRoundWin);
	HookEvent("teamplay_round_start", Event_OnRoundStart);
	HookEvent("player_death", Event_OnPlayerSpawn);
}

public void OnConfigsExecuted()
{
	char sSound[PLATFORM_MAX_PATH]; char sDownload[PLATFORM_MAX_PATH];
	
	GetConVarString(convar_Sound_RoundEnd, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		PrecacheSound(sSound);
		
		FormatEx(sDownload, sizeof(sDownload), "sound/%s", sSound);
		AddFileToDownloadsTable(sDownload);
	}
	
	GetConVarString(convar_Sound_RoundStart, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		PrecacheSound(sSound);
		
		FormatEx(sDownload, sizeof(sDownload), "sound/%s", sSound);
		AddFileToDownloadsTable(sDownload);
	}
	
	GetConVarString(convar_Sound_HumanDies, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		PrecacheSound(sSound);
		
		FormatEx(sDownload, sizeof(sDownload), "sound/%s", sSound);
		AddFileToDownloadsTable(sDownload);
	}
	
	GetConVarString(convar_Sound_LastManStanding, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		PrecacheSound(sSound);
		
		FormatEx(sDownload, sizeof(sDownload), "sound/%s", sSound);
		AddFileToDownloadsTable(sDownload);
	}
	
	GetConVarString(convar_Sound_ZombiesWin, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		PrecacheSound(sSound);
		
		FormatEx(sDownload, sizeof(sDownload), "sound/%s", sSound);
		AddFileToDownloadsTable(sDownload);
	}
	
	GetConVarString(convar_Sound_HumansWin, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		PrecacheSound(sSound);
		
		FormatEx(sDownload, sizeof(sDownload), "sound/%s", sSound);
		AddFileToDownloadsTable(sDownload);
	}
}

public void Event_OnRoundWin(Event event, const char[] name, bool dontBroadcast)
{
	char sSound[PLATFORM_MAX_PATH];
	GetConVarString(convar_Sound_RoundEnd, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		EmitSoundToAll(sSound);
	}
	
	switch (GetEventInt(event, "team"))
	{
		case 2:
		{
			GetConVarString(convar_Sound_ZombiesWin, sSound, sizeof(sSound));
			
			if (strlen(sSound) > 0)
			{
				EmitSoundToAll(sSound);
			}
		}
		case 3:
		{
			GetConVarString(convar_Sound_HumansWin, sSound, sizeof(sSound));
			
			if (strlen(sSound) > 0)
			{
				EmitSoundToAll(sSound);
			}
		}
	}
}

public void Event_OnRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	char sSound[PLATFORM_MAX_PATH];
	GetConVarString(convar_Sound_RoundStart, sSound, sizeof(sSound));
	
	if (strlen(sSound) > 0)
	{
		EmitSoundToAll(sSound);
	}
}

public void Event_OnPlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (client == 0 || client > MaxClients)
	{
		return;
	}
	
	if (!GoldC_IsZombie(client))
	{
		char sSound[PLATFORM_MAX_PATH];
		GetConVarString(convar_Sound_HumanDies, sSound, sizeof(sSound));
		
		if (strlen(sSound) > 0)
		{
			EmitSoundToAll(sSound);
		}
	}
}