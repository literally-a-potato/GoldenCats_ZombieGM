//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>
#include <tf2_stocks>

//External Includes
#include <sourcemod-misc>

//Our Includes
#include <goldc_zombies>

//ConVars
ConVar convar_Timer_Preparation;
ConVar convar_Timer_ZombieChoose;
ConVar convar_Timer_AFK;
ConVar convar_Timer_ZombieRespawn;

//Forwards
Handle g_hForward_OnInfected;

//Globals
Handle g_hTimer_Preparation;
Handle g_hTimer_ZombieChoose;

int g_iMoveTime[MAXPLAYERS + 1];

Handle g_hSync_CenterMessage;
Handle g_hSync_CenterMessage2;
Handle g_hSync_ZombiesRemaining;
Handle g_hSync_SurvivorsRemaining;

public Plugin myinfo =
{
	name = "[GoldC] Zombies",
	author = "Keith Warren (Drixevel)",
	description = "A basic zombies mode for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("goldc_zombies");
	CreateNative("GoldC_IsZombie", Native_IsZombie);
	g_hForward_OnInfected = CreateGlobalForward("GoldC_OnInfected", ET_Ignore, Param_Cell);
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoadTranslations("common.phrases");

	convar_Timer_Preparation = CreateConVar("sm_goldc_zombies_preparation_time", "40.0", "Time in seconds for preparation time to setup.", FCVAR_NOTIFY, true, 0.0);
	convar_Timer_ZombieChoose = CreateConVar("sm_goldc_zombies_zombiechoose_time", "10.0", "Time in seconds after preparation period to choose a zombie at random.", FCVAR_NOTIFY, true, 0.0);
	convar_Timer_AFK = CreateConVar("sm_goldc_zombies_afk_time", "420", "Time in seconds for clients who are AFK to get kicked.", FCVAR_NOTIFY, true, 0.0);
	convar_Timer_ZombieRespawn = CreateConVar("sm_goldc_zombies_respawn_time", "3.0", "Time in seconds for zombies to be respawned after death.", FCVAR_NOTIFY, true, 0.0);

	HookEvent("teamplay_round_start", Event_OnRoundStart);
	HookEvent("arena_round_start", Event_OnRoundStart);
	HookEvent("teamplay_round_active", Event_OnRoundActive);
	HookEvent("arena_round_start", Event_OnRoundActive);
	HookEvent("player_spawn", Event_OnPlayerSpawn);
	HookEvent("player_death", Event_OnPlayerDeath);
	
	g_hSync_CenterMessage = CreateHudSynchronizer();
	g_hSync_CenterMessage2 = CreateHudSynchronizer();
	g_hSync_ZombiesRemaining = CreateHudSynchronizer();
	g_hSync_SurvivorsRemaining = CreateHudSynchronizer();

	CreateTimer(5.0, Timer_CheckAFKs, _, TIMER_REPEAT);
}

public void OnPluginEnd()
{
	ClearSyncHudAll(g_hSync_CenterMessage);
	ClearSyncHudAll(g_hSync_CenterMessage2);
	ClearSyncHudAll(g_hSync_ZombiesRemaining);
	ClearSyncHudAll(g_hSync_SurvivorsRemaining);
}

public void OnConfigsExecuted()
{
	SetConVarFloat(FindConVar("mp_round_restart_delay"), 15.0);
	SetConVarInt(FindConVar("mp_timelimit"), 25);
	SetConVarInt(FindConVar("mp_maxrounds"), 10);
}

public void OnMapEnd()
{
	g_hTimer_Preparation = null;
}

public void OnClientDisconnect(int client)
{
	g_iMoveTime[client] = 0;
}

public void Event_OnRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && IsPlayerAlive(i) && TF2_GetClientTeam(i) == TFTeam_Red)
		{
			TF2_ChangeClientTeam(i, TFTeam_Blue);
		}
	}
}

public void Event_OnRoundActive(Event event, const char[] name, bool dontBroadcast)
{
	KillTimerSafe(g_hTimer_Preparation);
	g_hTimer_Preparation = CreateTimer(GetConVarFloat(convar_Timer_Preparation), Timer_EndPreparation, _, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_EndPreparation(Handle timer)
{
	KillTimerSafe(g_hTimer_ZombieChoose);
	g_hTimer_ZombieChoose = CreateTimer(GetConVarFloat(convar_Timer_ZombieChoose), Timer_ChooseRandomZombie, _, TIMER_FLAG_NO_MAPCHANGE);
}

public void Event_OnPlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int userid = GetEventInt(event, "userid");
	int client = GetClientOfUserId(userid);

	if (client == 0 || client > MaxClients || !IsClientInGame(client) || !IsPlayerAlive(client))
	{
		return;
	}

	if (GoldC_IsZombie(client))
	{
		//SetEntityHealth(client, TF2_GetPlayerClass(client) == TFClass_Heavy ? 750 : 550);
		CreateTimer(0.2, Timer_UpHealth, userid, TIMER_FLAG_NO_MAPCHANGE);

		SetEntityRenderMode(client, RENDER_TRANSCOLOR);
		SetEntityRenderColor(client, 6, 163, 6, 255);

		TF2_AddCondition(client, TFCond_RestrictToMelee, TFCondDuration_Infinite);

		int melee = GetPlayerWeaponSlot(client, 2);

		if (IsValidEntity(melee))
		{
			SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", melee);
		}
	}
}

public Action Timer_UpHealth(Handle timer, any data)
{
	int client = GetClientOfUserId(data);

	if (client > 0)
	{
		SetEntityHealth(client, TF2_GetPlayerClass(client) == TFClass_Heavy ? 750 : 550);
	}
}

public void Event_OnPlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int userid = GetEventInt(event, "userid");
	int client = GetClientOfUserId(userid);

	if (client == 0 || client > MaxClients || !IsClientInGame(client))
	{
		return;
	}

	if (TF2_GetClientTeam(client) == TFTeam_Blue)
	{
		TF2_ChangeClientTeam(client, TFTeam_Red);
		
		SetHudTextParams(0.0, 0.06, 5.0, 140, 255, 0, 255);
		ShowSyncHudTextAll(g_hSync_ZombiesRemaining, "Zombies: %i", GetTeamClientCount(2));
		
		SetHudTextParams(0.0, 0.03, 5.0, 0, 230, 255, 255);
		ShowSyncHudTextAll(g_hSync_SurvivorsRemaining, "Humans remaing: 0");

		Call_StartForward(g_hForward_OnInfected);
		Call_PushCell(client);
		Call_Finish();
		
		int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
		
		if (attacker > 0)
		{
			SetHudTextParams(-1.0, 0.25, 5.0, 255, 255, 0, 255);
			ShowSyncHudTextAll(g_hSync_CenterMessage, "%N got infected by %N", client, attacker);
		}
		
		int last;
		if (OneSurvivorLeft(last) && last > 0)
		{
			SetHudTextParams(-1.0, 0.25, 5.0, 255, 255, 255, 255);
			ShowSyncHudTextAll(g_hSync_CenterMessage2, "%N is the last survivor alive, good luck!", last);
		}
		else if (NoSurvivorsLeft())
		{
			SetHudTextParams(-1.0, 0.25, 5.0, 255, 255, 255, 255);
			ShowSyncHudTextAll(g_hSync_CenterMessage2, "Zombies Have Won the round! Prepare for the next one!");
		}
	}

	CreateTimer(GetConVarFloat(convar_Timer_ZombieRespawn), Timer_Respawn, userid, TIMER_FLAG_NO_MAPCHANGE);
}

bool OneSurvivorLeft(int& last)
{
	int count;
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && IsPlayerAlive(i) && TF2_GetClientTeam(i) == TFTeam_Blue)
		{
			last = i;
			count++;
		}
	}
	
	return count == 1;
}

bool NoSurvivorsLeft()
{
	int count;
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && IsPlayerAlive(i) && TF2_GetClientTeam(i) == TFTeam_Blue)
		{
			count++;
		}
	}
	
	return count == 0;
}

public Action Timer_Respawn(Handle timer, any data)
{
	int client = GetClientOfUserId(data);

	if (client > 0 && IsClientInGame(client) && !IsPlayerAlive(client))
	{
		TF2_RespawnPlayer(client);
	}
}

public Action Timer_ChooseRandomZombie(Handle timer)
{
	int zombies;
	int clients = GetClientCount(true);

	if (clients < 6)
	{
		zombies = 1;
	}
	else if (clients < 12)
	{
		zombies = 2;
	}
	else if (clients < 18)
	{
		zombies = 3;
	}
	else if (clients < 24 || clients <= 30)
	{
		zombies = 4;
	}
	else if (clients > 30)
	{
		zombies = 6;
	}

	for (int i = 0; i < zombies; i++)
	{
		int client;
		int attempts;

		while (client > 0 && TF2_GetClientTeam(client) != TFTeam_Red && attempts > 100)
		{
			client = GetRandomClient(true, true);
			attempts++;
		}

		if (attempts >= 100)
		{
			continue;
		}

		TF2_ChangeClientTeamEx(client, TFTeam_Red);

		SetEntityRenderMode(client, RENDER_TRANSCOLOR);
		SetEntityRenderColor(client, 6, 163, 6, 255);

		TF2_AddCondition(client, TFCond_RestrictToMelee, TFCondDuration_Infinite);

		int melee = GetPlayerWeaponSlot(client, 2);

		if (IsValidEntity(melee))
		{
			SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", melee);
		}

		Call_StartForward(g_hForward_OnInfected);
		Call_PushCell(client);
		Call_Finish();
	}
}

void TF2_ChangeClientTeamEx(int client, TFTeam team)
{
	int EntProp = GetEntProp(client, Prop_Send, "m_lifeState");
	SetEntProp(client, Prop_Send, "m_lifeState", 2);
	ChangeClientTeam(client, view_as<int>(team));
	SetEntProp(client, Prop_Send, "m_lifeState", EntProp);
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (client == 0 || client > MaxClients || IsFakeClient(client) || GetConVarInt(convar_Timer_AFK) == 0)
	{
		return Plugin_Continue;
	}

	if (buttons & IN_FORWARD || buttons & IN_BACK || buttons & IN_LEFT || buttons & IN_RIGHT || buttons & IN_JUMP || buttons & IN_DUCK)
	{
		g_iMoveTime[client] = GetTime();
	}

	return Plugin_Continue;
}

public Action Timer_CheckAFKs(Handle timer)
{
	int time = GetConVarInt(convar_Timer_AFK);

	if (time == 0)
	{
		return Plugin_Continue;
	}

	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i) && GetTime() - g_iMoveTime[i] > time)
		{
			KickClient(i, "You are currently AFK.");
		}
	}

	return Plugin_Continue;
}

public int Native_IsZombie(Handle plugin, int numParams)
{
	return view_as<bool>(TF2_GetClientTeam(GetNativeCell(1)) == TFTeam_Red);
}
