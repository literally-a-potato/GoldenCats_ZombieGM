//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>

//External Includes
#include <sourcemod-misc>

//Globals
float g_iTime;
Handle g_hTimer_Roundtimer;
Handle g_hSync_RoundTimer;

public Plugin myinfo =
{
	name = "[GoldC] Timer",
	author = "Keith Warren (Drixevel)",
	description = "A basic Timer plugin for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public void OnPluginStart()
{
	LoadTranslations("common.phrases");

	HookEvent("teamplay_round_start", Event_OnRoundStart);
	g_hSync_RoundTimer = CreateHudSynchronizer();
}

public void OnPluginEnd()
{
	g_hTimer_Roundtimer = null;
	ClearSyncHudAll(g_hSync_RoundTimer);
}

public void Event_OnRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	KillTimerSafe(g_hTimer_Roundtimer);
	g_hTimer_Roundtimer = CreateTimer(1.0, Timer_Countdown, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_Countdown(Handle timer)
{
	g_iTime--;
	
	SetHudTextParams(0.0, 0.0, 5.0, 255, 255, 255, 255);
	ShowSyncHudTextAll(g_hSync_RoundTimer, "Time: %i", g_iTime);
	
	if (g_iTime <= 0)
	{
		g_iTime = 0.0;
		g_hTimer_Roundtimer = null;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}