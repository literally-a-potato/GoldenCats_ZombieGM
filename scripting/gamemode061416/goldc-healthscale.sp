//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>
#include <tf2_stocks>
#include <sdkhooks>

//External Includes
#include <sourcemod-misc>

//Our Includes
#include <goldc_zombies>

//Globals
int g_iLastAction[MAXPLAYERS + 1];
int g_iMaxHealth[MAXPLAYERS + 1];

public Plugin myinfo =
{
	name = "[GoldC] Health Scale",
	author = "Keith Warren (Drixevel)",
	description = "A basic health scale plugin for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public void OnPluginStart()
{
	LoadTranslations("common.phrases");

	CreateTimer(1.0, Timer_GiveHealth, _, TIMER_REPEAT);

	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i))
		{
			OnClientPutInServer(i);
		}
	}
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamagePost, OnTakeDamagePost);
	SDKHook(client, SDKHook_GetMaxHealth, OnMaxHealth);
}

public void OnTakeDamagePost(int victim, int attacker, int inflictor, float damage, int damagetype, int weapon, const float damageForce[3], const float damagePosition[3], int damagecustom)
{
	g_iLastAction[victim] = GetTime();
}

public Action OnMaxHealth(int entity, int &maxhealth)
{
	/*if (GoldC_IsZombie(entity))
	{
		if (TF2_GetPlayerClass(entity) == TFClass_Heavy)
		{
			g_iMaxHealth[entity] = 750;
		}
		else
		{
			g_iMaxHealth[entity] = 550;
		}
	}
	else
	{
		g_iMaxHealth[entity] = 400;
	}*/

	g_iMaxHealth[entity] = maxhealth;
	return Plugin_Changed;
}

public void OnClientDisconnect(int client)
{
	g_iLastAction[client] = 0;
}

public Action Timer_GiveHealth(Handle timer)
{
	int time = GetTime();

	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && IsPlayerAlive(i) && time - g_iLastAction[i] > 2)
		{
			int give;

			if (GoldC_IsZombie(i))
			{
				give = 5;
			}
			else
			{
				give = 2;
			}

			int new_health = GetClientHealth(i) + give;

			if (new_health > g_iMaxHealth[i])
			{
				new_health = g_iMaxHealth[i];
			}

			SetEntityHealth(i, new_health);
		}
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (!GoldC_IsZombie(client) && GetClientSpeed(client) >= 1.0)
	{
		g_iLastAction[client] = GetTime();
	}

	return Plugin_Continue;
}

float GetClientSpeed(int client)
{
	float vecVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", vecVelocity);

	return SquareRoot(Pow(vecVelocity[0], 2.0) + Pow(vecVelocity[1], 2.0));
}
