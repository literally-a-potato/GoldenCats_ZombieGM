//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>
#include <tf2_stocks>

//External Includes
#include <sourcemod-misc>

//ConVars
ConVar convar_ForumURL;
ConVar convar_Points_Kill_Gain_Survivors;
ConVar convar_Points_Kill_Gain_Zombies;
ConVar convar_Points_Death_Lose_Survivors;
ConVar convar_Points_Death_Lose_Zombies;

//Globals
Handle g_hDatabase;

int g_iStat_TotalPoints[MAXPLAYERS + 1];
int g_iStat_TotalKills[MAXPLAYERS + 1];
int g_iStat_SurvivorKills[MAXPLAYERS + 1];
int g_iStat_ZombieKills[MAXPLAYERS + 1];
int g_iStat_TotalDeaths[MAXPLAYERS + 1];
int g_iStat_SurvivorDeaths[MAXPLAYERS + 1];
int g_iStat_ZombieDeaths[MAXPLAYERS + 1];
//float g_fStat_TotalTimePlayed[MAXPLAYERS + 1];

public Plugin myinfo =
{
	name = "[GoldC] Statistics",
	author = "Keith Warren (Drixevel)",
	description = "A basic statistics plugin for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public void OnPluginStart()
{
	LoadTranslations("common.phrases");
	
	convar_ForumURL = CreateConVar("sm_goldc_statistics_forum_url", "http://steamcommunity.com/groups/goldencatsstudios", "URL to the forum for the !forum command to use.", FCVAR_NOTIFY);
	convar_Points_Kill_Gain_Survivors = CreateConVar("sm_goldc_statistics_points_kill_gain_survivors", "0", "Points to give survivors who get kills.", FCVAR_NOTIFY);
	convar_Points_Kill_Gain_Zombies = CreateConVar("sm_goldc_statistics_points_kill_gain_zombies", "1", "Points to give zombies who get kills.", FCVAR_NOTIFY);
	convar_Points_Death_Lose_Survivors = CreateConVar("sm_goldc_statistics_points_death_lose_zombies", "5", "Points to remove from survivors who get killed.", FCVAR_NOTIFY);
	convar_Points_Death_Lose_Zombies = CreateConVar("sm_goldc_statistics_points_death_lose_zombies", "10", "Points to remove from zombies who get killed.", FCVAR_NOTIFY);
	
	RegConsoleCmd("sm_rank", Command_Rank);
	RegConsoleCmd("sm_top10", Command_Top10);
	RegConsoleCmd("sm_gameplay", Command_Gameplay);
	RegConsoleCmd("sm_forum", Command_Forum);
	
	HookEvent("player_death", Event_OnPlayerDeath);
}

public void OnConfigsExecuted()
{
	if (g_hDatabase == null)
	{
		SQL_TConnect(OnSQLConnect, "default");
	}
}

public void OnSQLConnect(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		LogError("Error connecting to database: %s", error);
		return;
	}
	
	if (g_hDatabase != null)
	{
		CloseHandle(hndl);
		return;
	}
	
	g_hDatabase = hndl;
	LogMessage("Database connection successful.");
}

public void Event_OnPlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	
	if (client > 0 && client <= MaxClients)
	{
		g_iStat_TotalDeaths[client]++;
		
		switch (GetClientTeam(attacker))
		{
			case 2:
			{
				g_iStat_ZombieDeaths[attacker]++;
				g_iStat_TotalPoints[client] -= GetConVarInt(convar_Points_Death_Lose_Zombies);
			}
			case 3:
			{
				g_iStat_SurvivorDeaths[attacker]++;
				g_iStat_TotalPoints[client] -= GetConVarInt(convar_Points_Death_Lose_Survivors);
			}
		}
	}
	
	if (attacker > 0 && attacker <= MaxClients)
	{
		g_iStat_TotalKills[attacker]++;
		
		switch (GetClientTeam(attacker))
		{
			case 2:
			{
				g_iStat_ZombieKills[attacker]++;
				g_iStat_TotalPoints[attacker] += GetConVarInt(convar_Points_Kill_Gain_Zombies);
			}
			case 3:
			{
				g_iStat_SurvivorKills[attacker]++;
				g_iStat_TotalPoints[attacker] += GetConVarInt(convar_Points_Kill_Gain_Survivors);
			}
		}
	}
}

public Action Command_Rank(int client, int args)
{
	return Plugin_Handled;
}

public Action Command_Top10(int client, int args)
{
	return Plugin_Handled;
}

public Action Command_Gameplay(int client, int args)
{
	return Plugin_Handled;
}

public Action Command_Forum(int client, int args)
{
	char sURL[256];
	GetConVarString(convar_ForumURL, sURL, sizeof(sURL));
	
	ShowMOTDPanel(client, "Forum", sURL, MOTDPANEL_TYPE_URL);
	return Plugin_Handled;
}