//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>
#include <sdkhooks>
#include <tf2_stocks>

//External Includes
#include <sourcemod-misc>

//Our Includes
#include <goldc_zombies>

//ConVars
ConVar convar_PointsPerKill;
ConVar convar_HealthRegenTimer;

//Globals
int g_iPerkPoints[MAXPLAYERS + 1];
int g_iMedikitsUsed[MAXPLAYERS + 1];
bool g_bRegenerateHealth[MAXPLAYERS + 1];
bool g_bSpeedBuff[MAXPLAYERS + 1];
bool g_bAlwaysCrit[MAXPLAYERS + 1];
int g_iMaxHealth[MAXPLAYERS + 1];

Handle g_hRegenTimer;
Handle g_hSync_PerkPoints;

public Plugin myinfo =
{
	name = "[GoldC] Perks",
	author = "Keith Warren (Drixevel)",
	description = "A basic zombies mode for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public void OnPluginStart()
{
	LoadTranslations("common.phrases");

	convar_PointsPerKill = CreateConVar("sm_goldc_perks_pointsperkill", "2");
	convar_HealthRegenTimer = CreateConVar("sm_goldc_perks_healthregentimer", "1.0", "", _, true, 0.0);

	HookConVarChange(convar_HealthRegenTimer, OnConVarChanged_HealthRegenTimer);

	HookEvent("player_death", Event_OnPlayerDeath);
	HookEvent("teamplay_round_win", Event_OnRoundEnd);
	
	g_hSync_PerkPoints = CreateHudSynchronizer();

	RegConsoleCmd("sm_regen", Command_Regen);
	RegConsoleCmd("sm_speed", Command_Speed);
	RegConsoleCmd("sm_crits", Command_Crits);
	RegConsoleCmd("sm_medikit", Command_Medikit);

	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i))
		{
			OnClientPutInServer(i);
		}
	}
}

public void OnPluginEnd()
{
	ClearSyncHudAll(g_hSync_PerkPoints);
}

public void OnConVarChanged_HealthRegenTimer(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if (StrEqual(oldValue, newValue))
	{
		return;
	}
	
	KillTimerSafe(g_hRegenTimer);
	g_hRegenTimer = CreateTimer(GetConVarFloat(convar_HealthRegenTimer), Timer_RegenerateHealth, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public void OnConfigsExecuted()
{
	KillTimerSafe(g_hRegenTimer);
	g_hRegenTimer = CreateTimer(GetConVarFloat(convar_HealthRegenTimer), Timer_RegenerateHealth, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public void OnMapEnd()
{
	ResetVariables();
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_GetMaxHealth, OnMaxHealth);
}

public Action OnMaxHealth(int entity, int &maxhealth)
{
	g_iMaxHealth[entity] = maxhealth;
	return Plugin_Continue;
}

public Action Timer_RegenerateHealth(Handle timer)
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && IsPlayerAlive(i) && g_bRegenerateHealth[i])
		{
			int new_health = GetClientHealth(i) + 10;

			if (new_health > g_iMaxHealth[i])
			{
				new_health = g_iMaxHealth[i];
			}

			SetEntityHealth(i, new_health);
		}
	}
}

public void Event_OnPlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int userid = GetEventInt(event, "userid");
	int client = GetClientOfUserId(userid);

	int userida = GetEventInt(event, "attacker");
	int clienta = GetClientOfUserId(userida);

	if (client == 0 || client > MaxClients || !IsClientInGame(client) || !IsPlayerAlive(client) || clienta == 0 || clienta > MaxClients || !IsClientInGame(clienta) || !IsPlayerAlive(clienta))
	{
		return;
	}

	if (TF2_GetClientTeam(client) == TFTeam_Red && TF2_GetClientTeam(clienta) == TFTeam_Blue)
	{
		g_iPerkPoints[clienta] += GetConVarInt(convar_PointsPerKill);
		UpdatePerksHud(clienta);
		PrintToChat(clienta, "You have gained %i points. (total: %i)", GetConVarInt(convar_PointsPerKill), g_iPerkPoints[clienta]);
	}
}

public void Event_OnRoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	ResetVariables();
}

void ResetVariables()
{
	for (int i = 1; i <= MaxClients; i++)
	{
		g_iPerkPoints[i] = 0;
		UpdatePerksHud(i);
		g_iMedikitsUsed[i] = 0;
		g_bRegenerateHealth[i] = false;
		g_bSpeedBuff[i] = false;
		g_bAlwaysCrit[i] = false;
	}
}

public void TF2_OnConditionRemoved(int client, TFCond condition)
{
	if (condition == TFCond_SpeedBuffAlly && g_bSpeedBuff[client])
	{
		TF2_AddCondition(client, TFCond_SpeedBuffAlly, TFCondDuration_Infinite);
	}
}

public Action TF2_CalcIsAttackCritical(int client, int weapon, char[] weaponname, bool &result)
{
	if (g_bAlwaysCrit[client])
	{
		result = true;
		return Plugin_Changed;
	}

	return Plugin_Continue;
}

public Action Command_Regen(int client, int args)
{
	if (g_iPerkPoints[client] < 180)
	{
		PrintToChat(client, "You have to have 180 perk points to purchase regen.");
		return Plugin_Handled;
	}

	g_iPerkPoints[client] -= 180;
	PrintToChat(client, "You have purchased the regen perk for 180 points.");
	UpdatePerksHud(client);

	g_bRegenerateHealth[client] = true;

	return Plugin_Handled;
}

public Action Command_Speed(int client, int args)
{
	if (g_iPerkPoints[client] < 100)
	{
		PrintToChat(client, "You have to have 100 perk points to purchase speed.");
		return Plugin_Handled;
	}

	g_iPerkPoints[client] -= 100;
	PrintToChat(client, "You have purchased the speed perk for 100 points.");
	UpdatePerksHud(client);

	TF2_AddCondition(client, TFCond_SpeedBuffAlly, TFCondDuration_Infinite);
	g_bSpeedBuff[client] = true;

	return Plugin_Handled;
}

public Action Command_Crits(int client, int args)
{
	if (g_iPerkPoints[client] < 160)
	{
		PrintToChat(client, "You have to have 160 perk points to purchase crits.");
		return Plugin_Handled;
	}

	g_iPerkPoints[client] -= 160;
	PrintToChat(client, "You have purchased the crits perk for 160 points.");
	UpdatePerksHud(client);

	g_bAlwaysCrit[client] = true;

	return Plugin_Handled;
}

public Action Command_Medikit(int client, int args)
{
	if (g_iMedikitsUsed[client] >= 3)
	{
		PrintToChat(client, "You are limited to 3 medikits per round.");
		return Plugin_Handled;
	}

	g_iMedikitsUsed[client]++;
	PrintToChat(client, "You have used a medikit this round, you have %i left.", 3 - g_iMedikitsUsed[client]);

	TF2_RegeneratePlayer(client);

	return Plugin_Handled;
}

void UpdatePerksHud(int client)
{
	SetHudTextParams(0.0, 0.09, 5.0, 255, 255, 0, 255);
	ShowSyncHudText(client, g_hSync_PerkPoints, "Perk points: %i", g_iPerkPoints[client]);
}