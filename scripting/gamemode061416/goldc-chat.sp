//Pragma
#pragma semicolon 1
#pragma newdecls required

//Defines

//Includes
#include <sourcemod>
#include <tf2_stocks>
#include <clientprefs>

//External Includes
#include <sourcemod-misc>

//Required Includes
//#include <chat-processor>

//Globals
Handle g_hArray_Adverts;
Handle g_hCookie_CenterDeathMessages;

int g_iCurrentAd;

public Plugin myinfo =
{
	name = "[GoldC] Chat",
	author = "Keith Warren (Drixevel)",
	description = "A basic chat plugin for GoldC.",
	version = "1.0.0",
	url = "http://www.drixevel.com/"
};

public void OnPluginStart()
{
	LoadTranslations("common.phrases");
	
	HookEvent("player_death", Event_OnPlayerDeath);
	
	g_hCookie_CenterDeathMessages = RegClientCookie("center_death_messages", "Shows the center messages on death or kills.", CookieAccess_Protected);
	
	RegConsoleCmd("sm_deathmessages", Command_ToggleDeathMessages, "Toggle death messages when you kill someone or are killed.");
	
	g_hArray_Adverts = CreateArray(ByteCountToCells(255));
	CreateTimer(300.0, Timer_DisplayAd, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public void OnConfigsExecuted()
{
	ParseAdvertisements("configs/goldc/advertisements.cfg");
}

public void Event_OnPlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int userid_client = GetEventInt(event, "userid");
	int userid_attack = GetEventInt(event, "attacker");
	
	int client = GetClientOfUserId(userid_client);
	int attacker = GetClientOfUserId(userid_attack);
	
	if (client == 0 || client > MaxClients || attacker == 0 || attacker > MaxClients)
	{
		return;
	}
	
	char sValue[8];
	
	GetClientCookie(client, g_hCookie_CenterDeathMessages, sValue, sizeof(sValue));
	if (StrEqual(sValue, "1"))
	{
		PrintCenterText(client, "You have been killed by %N!", attacker);
	}
	
	GetClientCookie(attacker, g_hCookie_CenterDeathMessages, sValue, sizeof(sValue));
	if (StrEqual(sValue, "1"))
	{
		PrintCenterText(attacker, "You have killed %N!", client);
	}
}

public Action Command_ToggleDeathMessages(int client, int args)
{
	char sValue[8];
	GetClientCookie(client, g_hCookie_CenterDeathMessages, sValue, sizeof(sValue));
	SetClientCookie(client, g_hCookie_CenterDeathMessages, StrEqual(sValue, "1") ? "0" : "1");
	PrintToChat(client, "Death Messages: %s", StrEqual(sValue, "1") ? "OFF" : "ON");
	return Plugin_Handled;
}

public Action Timer_DisplayAd(Handle timer)
{
	if (GetArraySize(g_hArray_Adverts) == 0)
	{
		return Plugin_Continue;
	}
	
	char sAdvert[255];
	GetArrayString(g_hArray_Adverts, g_iCurrentAd, sAdvert, sizeof(sAdvert));
	g_iCurrentAd++;
	
	PrintToChatAll(sAdvert);
	
	if (g_iCurrentAd > GetArraySize(g_hArray_Adverts) - 1)
	{
		g_iCurrentAd = 0;
	}
	
	return Plugin_Continue;
}

void ParseAdvertisements(const char[] config)
{
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), config);
	
	KeyValues kv = CreateKeyValues("goldc_advertisements");
	
	if (FileToKeyValues(kv, sPath) && KvGotoFirstSubKey(kv, false))
	{
		ClearArray(g_hArray_Adverts);
		g_iCurrentAd = 0;
		
		do
		{
			char sVoid[8];
			KvGetSectionName(kv, sVoid, sizeof(sVoid));
			
			char sAdvert[255];
			KvGetString(kv, NULL_STRING, sAdvert, sizeof(sAdvert));
			PushArrayString(g_hArray_Adverts, sAdvert);
		}
		while (KvGotoNextKey(kv, false));
	}
	
	CloseHandle(kv);
	LogMessage("Successfully parsed advertisements.");
}