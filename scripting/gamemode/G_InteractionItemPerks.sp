public void OnConVarChanged_HealthRegenTimer (ConVar convar, const char[] oldValue, const char[] newValue)
{
    if (StrEqual (oldValue, newValue))
    {
        return;
    }
    
    KillTimer (g_hRegenTimer);
    g_hRegenTimer = CreateTimer (GetConVarFloat (convar_HealthRegenTimer), Timer_RegenerateHealth, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public Action OnItemMaxHealth (int entity, int &maxhealth)
{
    g_iMaxHealth[entity] = maxhealth;
    return Plugin_Continue;
}

public Action Timer_RegenerateHealth (Handle timer)
{
    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame (i) && IsPlayerAlive (i) && g_bRegenerateHealth[i])
        {
            int new_health = GetClientHealth (i) + 10;

            if (new_health > g_iMaxHealth[i])
            {
                new_health = g_iMaxHealth[i];
            }

            SetEntityHealth (i, new_health);
        }
    }
}

public void Event_OnPlayerDeathItem (Event event, const char[] name, bool dontBroadcast)
{
    int userid = GetEventInt (event, "userid");
    int client = GetClientOfUserId (userid);

    int userida = GetEventInt (event, "attacker");
    int clienta = GetClientOfUserId (userida);

    if (client == 0 || client > MaxClients || !IsClientInGame (client) || !IsPlayerAlive (client) || clienta == 0 || clienta > MaxClients || !IsClientInGame (clienta) || !IsPlayerAlive (clienta))
    {
        return;
    }

    if (TF2_GetClientTeam (client) == TFTeam_Red && TF2_GetClientTeam (clienta) == TFTeam_Blue)
    {
        g_iPerkPoints[clienta] += GetConVarInt (convar_PointsPerKill);
        UpdatePerksHud (clienta);
        PrintToChat (clienta, "You have gained %i points. (total: %i)", GetConVarInt (convar_PointsPerKill), g_iPerkPoints[clienta]);
    }
}

public void Event_OnRoundEnd (Event event, const char[] name, bool dontBroadcast)
{
    ResetVariables ();
}

void ResetVariables ()
{
    for (int i = 1; i <= MaxClients; i++)
    {
        g_iPerkPoints[i] = 0;
        UpdatePerksHud (i);
        g_iMedikitsUsed[i] = 0;
        g_bRegenerateHealth[i] = false;
        g_bSpeedBuff[i] = false;
        g_bAlwaysCrit[i] = false;
    }
}

public Action Command_Regen (int client, int args)
{

    int iHealthRegenCost = GetConVarInt (convar_HealthRegenCost);

    if (g_iPerkPoints[client] < iHealthRegenCost)
    {
        PrintToChat (client, "You have to have %i perk points to purchase regen.", iHealthRegenCost);
        return Plugin_Handled;
    }

    g_iPerkPoints [client] -= iHealthRegenCost;

    PrintToChat     (client, "You have purchased the regen perk for %i points.", iHealthRegenCost);
    UpdatePerksHud     (client);

    g_bRegenerateHealth [client] = true;

    return Plugin_Handled;
}

public Action Command_Speed (int client, int args)
{

    int iSpeedCost = GetConVarInt (convar_SpeedCost);

    if (g_iPerkPoints[client] < iSpeedCost)
    {
        PrintToChat (client, "You have to have %i perk points to purchase speed.", iSpeedCost);
        return Plugin_Handled;
    }

    g_iPerkPoints[client] -= iSpeedCost;

    PrintToChat     (client, "You have purchased the speed perk for %i points.", iSpeedCost);
    UpdatePerksHud     (client);

    TF2_AddCondition (client, TFCond_SpeedBuffAlly, TFCondDuration_Infinite);
    g_bSpeedBuff[client] = true;

    return Plugin_Handled;
}

public Action Command_Crits (int client, int args)
{

    int iCritsCost = GetConVarInt (convar_CritsCost);

    if (g_iPerkPoints[client] < iCritsCost)
    {
        PrintToChat (client, "You have to have %i perk points to purchase crits.", iCritsCost);
        return Plugin_Handled;
    }

    g_iPerkPoints[client] -= iCritsCost;

    PrintToChat     (client, "You have purchased the crits perk for %i points.", iCritsCost);
    UpdatePerksHud     (client);

    g_bAlwaysCrit[client] = true;

    return Plugin_Handled;
}

public Action Command_Medikit (int client, int args)
{
    if (g_iMedikitsUsed[client] >= 3)
    {
        PrintToChat (client, "You are limited to 3 medikits per round.");
        return Plugin_Handled;
    }

    g_iMedikitsUsed[client]++;
    PrintToChat (client, "You have used a medikit this round, you have %i left.", 3 - g_iMedikitsUsed[client]);

    TF2_RegeneratePlayer (client);

    return Plugin_Handled;
}

void UpdatePerksHud (int client)
{
    SetHudTextParams (0.0, 0.09, 5.0, 255, 255, 0, 255);
    ShowSyncHudText (client, g_hSync_PerkPoints, "Perk points: %i", g_iPerkPoints[client]);
}