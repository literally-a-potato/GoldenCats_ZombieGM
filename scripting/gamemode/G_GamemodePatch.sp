#pragma newdecls optional

public ClearSyncHudAll (Handle SyncHud_Handle) {
    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame (i))
        {
            ClearSyncHud (i, SyncHud_Handle);
        }
    }
}



public ShowSyncHudTextAll (Handle SyncHud_Handle, const String:strMessage[], any ...) {
    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame (i))
        {
            ShowSyncHudText (i, SyncHud_Handle, strMessage);
        }
    }
}


public KillTimerSafe (Handle Timer) {
    // Yeah I'm not sure what they mean by 'safe' but here's for backwards compadibility.
    KillTimer (Timer);
}

public int GetRandomClient (bool I, bool dunno) {
    int iMax = GetMaxClients ();
    return GetRandomInt (1, iMax);
}


public int GetActiveWeapon (int client) {
    new String:strWeaponClass[32];
    GetClientWeapon (client, strWeaponClass, sizeof (strWeaponClass));

    for (int i = 0; i <= 5; i++)
    {
        new String:strWeaponSlotClass[32];
        GetEntityClassname (GetPlayerWeaponSlot (client, i), strWeaponSlotClass, sizeof (strWeaponSlotClass));

        if (StrEqual (strWeaponSlotClass, strWeaponClass)) {
            return GetPlayerWeaponSlot (client, i);
        }
    }

    return -1
}