public void OnSQLConnect (Handle owner, Handle hndl, const char[] error, any data)
{
    if (hndl == null)
    {
        LogError ("Error connecting to database: %s", error);
        return;
    }
    
    if (g_hDatabase != null)
    {
        CloseHandle (hndl);
        return;
    }
    
    g_hDatabase = hndl;
    LogMessage ("Database connection successful.");
}

public void Event_OnPlayerDeathStats (Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId (GetEventInt (event, "userid"));
    int attacker = GetClientOfUserId (GetEventInt (event, "attacker"));
    
    if (client > 0 && client <= MaxClients)
    {
        g_iStat_TotalDeaths[client]++;
        
        switch (GetClientTeam (attacker))
        {
            case 2:
            {
                g_iStat_ZombieDeaths[attacker]++;
                g_iStat_TotalPoints[client] -= GetConVarInt (convar_Points_Death_Lose_Zombies);
            }
            case 3:
            {
                g_iStat_SurvivorDeaths[attacker]++;
                g_iStat_TotalPoints[client] -= GetConVarInt (convar_Points_Death_Lose_Survivors);
            }
        }
    }
    
    if (attacker > 0 && attacker <= MaxClients)
    {
        g_iStat_TotalKills[attacker]++;
        
        switch (GetClientTeam (attacker))
        {
            case 2:
            {
                g_iStat_ZombieKills[attacker]++;
                g_iStat_TotalPoints[attacker] += GetConVarInt (convar_Points_Kill_Gain_Zombies);
            }
            case 3:
            {
                g_iStat_SurvivorKills[attacker]++;
                g_iStat_TotalPoints[attacker] += GetConVarInt (convar_Points_Kill_Gain_Survivors);
            }
        }
    }
}

public Action Command_Rank (int client, int args)
{
    return Plugin_Handled;
}

public Action Command_Top10 (int client, int args)
{
    return Plugin_Handled;
}

public Action Command_Gameplay (int client, int args)
{
    return Plugin_Handled;
}

public Action Command_Forum (int client, int args)
{
    char sURL[256];
    GetConVarString (convar_ForumURL, sURL, sizeof (sURL));
    
    ShowMOTDPanel (client, "Forum", sURL, MOTDPANEL_TYPE_URL);
    return Plugin_Handled;
}