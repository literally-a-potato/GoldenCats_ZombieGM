void ParseSkinsConfig (const char[] config)
{
    char sPath[PLATFORM_MAX_PATH];
    BuildPath (Path_SM, sPath, sizeof (sPath), config);
    
    KeyValues kv = CreateKeyValues ("goldc_skins");
    
    if (!FileToKeyValues (kv, sPath))
    {
        return;
    }
    
    if (KvJumpToKey (kv, "humans") && KvGotoFirstSubKey (kv))
    {
        ClearArray (g_hArray_Skin_Names_Humans);
        ClearTrie (g_hTrie_Skin_Model_Humans);
        ClearTrie (g_hTrie_Skin_Class_Humans);
        
        do
        {
            char sName[MAX_NAME_LENGTH];
            KvGetSectionName (kv, sName, sizeof (sName));
            PushArrayString (g_hArray_Skin_Names_Humans, sName);
            
            char sModel[PLATFORM_MAX_PATH];
            KvGetString (kv, "model", sModel, sizeof (sModel));
            SetTrieString (g_hTrie_Skin_Model_Humans, sName, sModel);
            PrecacheModel (sModel);
            
            char sClass[32];
            KvGetString (kv, "class", sClass, sizeof (sClass));
            SetTrieString (g_hTrie_Skin_Class_Humans, sName, sClass);
            
            
        }
        while (KvGotoNextKey (kv));
        
        KvRewind (kv);
    }
    
    if (KvJumpToKey (kv, "zombies") && KvGotoFirstSubKey (kv))
    {
        ClearArray (g_hArray_Skin_Names_Zombies);
        ClearTrie (g_hTrie_Skin_Model_Zombies);
        ClearTrie (g_hTrie_Skin_Class_Zombies);
        
        do
        {
            char sName[MAX_NAME_LENGTH];
            KvGetSectionName (kv, sName, sizeof (sName));
            PushArrayString (g_hArray_Skin_Names_Zombies, sName);
            
            char sModel[PLATFORM_MAX_PATH];
            KvGetString (kv, "model", sModel, sizeof (sModel));
            SetTrieString (g_hTrie_Skin_Model_Zombies, sName, sModel);
            PrecacheModel (sModel);
            
            char sClass[32];
            KvGetString (kv, "class", sClass, sizeof (sClass));
            SetTrieString (g_hTrie_Skin_Class_Zombies, sName, sClass);
        }
        while (KvGotoNextKey (kv));
        
        KvRewind (kv);
    }
    
    CloseHandle (kv);
    LogMessage ("Successfully parsed the skins configurations.");
}

public Action Command_Skins (int client, int args)
{
    Menu menu = CreateMenu (MenuHandler_Skins);
    SetMenuTitle (menu, "Pick a skin:");
    
    bool zombie = GoldC_IsZombie (client);
    
    for (int i = 0; i < GetArraySize (zombie ? g_hArray_Skin_Names_Zombies : g_hArray_Skin_Names_Humans); i++)
    {
        char sName[MAX_NAME_LENGTH];
        GetArrayString (zombie ? g_hArray_Skin_Names_Zombies : g_hArray_Skin_Names_Humans, i, sName, sizeof (sName));
        
        char sClass[32];
        GetTrieString (zombie ? g_hTrie_Skin_Class_Zombies : g_hTrie_Skin_Class_Humans, sName, sClass, sizeof (sClass));
        
        if (TF2_GetPlayerClass (client) != TF2_GetClass (sClass))
        {
            continue;
        }
        
        char sModel[PLATFORM_MAX_PATH];
        GetTrieString (zombie ? g_hTrie_Skin_Model_Zombies : g_hTrie_Skin_Model_Humans, sName, sModel, sizeof (sModel));
        
        AddMenuItem (menu, sModel, sName);
    }
    
    if (GetMenuItemCount (menu) == 0)
    {
        AddMenuItem (menu, "", "[No Skins Available]", ITEMDRAW_DISABLED);
    }
    
    DisplayMenu (menu, client, MENU_TIME_FOREVER);
    return Plugin_Handled;
}

public int MenuHandler_Skins (Menu menu, MenuAction action, int param1, int param2)
{
    switch (action)
    {
        case MenuAction_Select:
        {
            char sModel[PLATFORM_MAX_PATH]; char sName[MAX_NAME_LENGTH];
            GetMenuItem (menu, param2, sModel, sizeof (sModel), _, sName, sizeof (sName));
            
            if (strlen (sModel) > 0 && IsModelPrecached (sModel))
            {
                SetEntityModel (param1, sModel);
            }
            
            PrintToChat (param1, "You have equipped the skin: %s", sName);
        }
        
        case MenuAction_End:
        {
            CloseHandle (menu);
        }
    }
}