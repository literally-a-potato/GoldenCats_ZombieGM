public void OnPluginStart ()
{
    LoadTranslations ("common.phrases");    
    
    g_hArray_Adverts                    = CreateArray (ByteCountToCells (255));
    g_hArray_Skin_Names_Humans          = CreateArray (ByteCountToCells (MAX_NAME_LENGTH));
    g_hArray_Skin_Names_Zombies         = CreateArray (ByteCountToCells (MAX_NAME_LENGTH));
    g_hSync_PerkPoints                  = CreateHudSynchronizer ();
    g_hSync_RoundTimer                  = CreateHudSynchronizer ();
    g_hSync_CenterMessage               = CreateHudSynchronizer ();
    g_hSync_CenterMessage2              = CreateHudSynchronizer ();
    g_hSync_ZombiesRemaining            = CreateHudSynchronizer ();
    g_hSync_SurvivorsRemaining          = CreateHudSynchronizer ();
    g_hTrie_Skin_Model_Humans           = CreateTrie ();
    g_hTrie_Skin_Class_Humans           = CreateTrie ();
    g_hTrie_Skin_Model_Zombies          = CreateTrie ();
   
    g_hTrie_Skin_Class_Zombies          = CreateTrie ();
    g_hTrie_WeaponChanges_Primary       = CreateTrie ();
    g_hTrie_WeaponChanges_Secondary     = CreateTrie ();
    g_hTrie_WeaponChanges_Melee         = CreateTrie ();
    g_hCookie_CenterDeathMessages       = RegClientCookie ("center_death_messages", "Shows the center messages on death or kills.", CookieAccess_Protected);

    CreateTimer (300.0, Timer_DisplayAd,  _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
    CreateTimer (1.0,   Timer_GiveHealth, _, TIMER_REPEAT);
    CreateTimer (5.0,   Timer_CheckAFKs,  _, TIMER_REPEAT);

    HookEvent ("player_death",           Event_OnPlayerDeath);
    HookEvent ("player_death",           Event_OnPlayerDeathItem);
    HookEvent ("player_death",           Event_OnPlayerSpawnSound);
    HookEvent ("player_death",           Event_OnPlayerDeathStats);
    HookEvent ("player_spawn",           Event_OnPlayerSpawn);
    HookEvent ("teamplay_round_win",     Event_OnRoundEnd);
    HookEvent ("teamplay_round_win",     Event_OnRoundWin);
    HookEvent ("teamplay_round_start",   Event_OnRoundStart);
    HookEvent ("teamplay_round_start",   Event_OnRoundStart);
    HookEvent ("player_spawn",           Event_OnPlayerSpawn);
    HookEvent ("teamplay_round_start",   Event_OnRoundStartZombies);
    HookEvent ("arena_round_start",      Event_OnRoundStartZombies);
    HookEvent ("teamplay_round_active",  Event_OnRoundActive);
    HookEvent ("arena_round_start",      Event_OnRoundActive);
    HookEvent ("player_spawn",           Event_OnPlayerSpawnZombies);
    HookEvent ("player_death",           Event_OnPlayerDeathZombies);
    
    RegConsoleCmd ("sm_rank",           Command_Rank,                   "");
    RegConsoleCmd ("sm_top10",          Command_Top10,                  "");
    RegConsoleCmd ("sm_gameplay",       Command_Gameplay,               "");
    RegConsoleCmd ("sm_forum",          Command_Forum,                  "");
    RegConsoleCmd ("sm_deathmessages",  Command_ToggleDeathMessages,    "Toggle death messages when you kill someone or are killed.");
    RegConsoleCmd ("sm_regen",          Command_Regen,                  "");
    RegConsoleCmd ("sm_speed",          Command_Speed,                  "");
    RegConsoleCmd ("sm_crits",          Command_Crits,                  "");
    RegConsoleCmd ("sm_medikit",        Command_Medikit,                "");
    RegConsoleCmd ("sm_skins",          Command_Skins,                  "Display the GoldC skins menu.");
    AddCommandListener (Listern_OnJoinClass, "joinclass");

    convar_PointsPerKill                = CreateConVar ("sm_goldc_perks_pointsperkill",                     "2");
    convar_HealthRegenTimer             = CreateConVar ("sm_goldc_perks_healthregen_timer",                 "1.0");
    convar_HealthRegenCost              = CreateConVar ("sm_goldc_perks_cost_healthregen",                  "180");
    convar_SpeedCost                    = CreateConVar ("sm_goldc_perks_cost_speed",                        "100");
    convar_CritsCost                    = CreateConVar ("sm_goldc_perks_cost_crits",                        "160");
    convar_Limit_Scout                  = CreateConVar ("sm_goldc_classlimiter_scout",                      "-1");
    convar_Limit_Soldier                = CreateConVar ("sm_goldc_classlimiter_soldier",                    "-1");
    convar_Limit_Pyro                   = CreateConVar ("sm_goldc_classlimiter_pyro",                       "-1");
    convar_Limit_Demoman                = CreateConVar ("sm_goldc_classlimiter_demoman",                    "4");
    convar_Limit_Heavy                  = CreateConVar ("sm_goldc_classlimiter_heavy",                      "3");
    convar_Limit_Engineer               = CreateConVar ("sm_goldc_classlimiter_engineer",                   "3");
    convar_Limit_Medic                  = CreateConVar ("sm_goldc_classlimiter_medic",                      "-1");
    convar_Limit_Sniper                 = CreateConVar ("sm_goldc_classlimiter_ssniper",                    "-1");
    convar_Limit_Spy                    = CreateConVar ("sm_goldc_classlimiter_spy",                        "3");
    convar_Sound_RoundEnd               = CreateConVar ("sm_goldc_sounds_roundend",                         "");
    convar_Sound_RoundStart             = CreateConVar ("sm_goldc_sounds_roundstart",                       "");
    convar_Sound_HumanDies              = CreateConVar ("sm_goldc_sounds_humandies",                        "");
    convar_Sound_LastManStanding        = CreateConVar ("sm_goldc_sounds_lastmanstanding",                  "");
    convar_Sound_ZombiesWin             = CreateConVar ("sm_goldc_sounds_zombieswin",                       "");
    convar_Sound_HumansWin              = CreateConVar ("sm_goldc_sounds_humanswin",                        "");
    convar_ForumURL                     = CreateConVar ("sm_goldc_statistics_forum_url",                    "http://steamcommunity.com/groups/goldencatsstudios", "URL to the forum for the !forum command to use.",    FCVAR_NOTIFY);
    convar_Points_Kill_Gain_Survivors   = CreateConVar ("sm_goldc_statistics_points_kill_gain_survivors",   "0", "Points to give survivors who get kills.",                                                             FCVAR_NOTIFY);
    convar_Points_Kill_Gain_Zombies     = CreateConVar ("sm_goldc_statistics_points_kill_gain_zombies",     "1", "Points to give zombies who get kills.",                                                               FCVAR_NOTIFY);
    convar_Points_Death_Lose_Survivors  = CreateConVar ("sm_goldc_statistics_points_death_lose_zombies",    "5", "Points to remove from survivors who get killed.",                                                     FCVAR_NOTIFY);
    convar_Points_Death_Lose_Zombies    = CreateConVar ("sm_goldc_statistics_points_death_lose_zombies",    "10", "Points to remove from zombies who get killed.",                                                      FCVAR_NOTIFY);
    convar_Timer_Preparation            = CreateConVar ("sm_goldc_zombies_preparation_time",                "40.0", "Time in seconds for preparation time to setup.",                                                   FCVAR_NOTIFY, true, 0.0);
    convar_Timer_ZombieChoose           = CreateConVar ("sm_goldc_zombies_zombiechoose_time",               "10.0", "Time in seconds after preparation period to choose a zombie at random.",                           FCVAR_NOTIFY, true, 0.0);
    convar_Timer_AFK                    = CreateConVar ("sm_goldc_zombies_afk_time",                        "420",  "Time in seconds for clients who are AFK to get kicked.",                                           FCVAR_NOTIFY, true, 0.0);
    convar_Timer_ZombieRespawn          = CreateConVar ("sm_goldc_zombies_respawn_time",                    "3.0",  "Time in seconds for zombies to be respawned after death.",                                         FCVAR_NOTIFY, true, 0.0);
    HookConVarChange                     (convar_HealthRegenTimer, OnConVarChanged_HealthRegenTimer);


    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame (i))
        {
            OnClientPutInServer (i);
        }
    }
}

public void OnConfigsExecuted ()
{
    KillTimer (g_hRegenTimer);
    g_hRegenTimer = CreateTimer (GetConVarFloat (convar_HealthRegenTimer), Timer_RegenerateHealth, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);

    char sSound[PLATFORM_MAX_PATH]; char sDownload[PLATFORM_MAX_PATH];
    
    GetConVarString (convar_Sound_RoundEnd, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        PrecacheSound (sSound);
        
        FormatEx (sDownload, sizeof (sDownload), "sound/%s", sSound);
        AddFileToDownloadsTable (sDownload);
    }
    
    GetConVarString (convar_Sound_RoundStart, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        PrecacheSound (sSound);
        
        FormatEx (sDownload, sizeof (sDownload), "sound/%s", sSound);
        AddFileToDownloadsTable (sDownload);
    }
    
    GetConVarString (convar_Sound_HumanDies, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        PrecacheSound (sSound);
        
        FormatEx (sDownload, sizeof (sDownload), "sound/%s", sSound);
        AddFileToDownloadsTable (sDownload);
    }
    
    GetConVarString (convar_Sound_LastManStanding, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        PrecacheSound (sSound);
        
        FormatEx (sDownload, sizeof (sDownload), "sound/%s", sSound);
        AddFileToDownloadsTable (sDownload);
    }
    
    GetConVarString (convar_Sound_ZombiesWin, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        PrecacheSound (sSound);
        
        FormatEx (sDownload, sizeof (sDownload), "sound/%s", sSound);
        AddFileToDownloadsTable (sDownload);
    }
    
    GetConVarString (convar_Sound_HumansWin, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        PrecacheSound (sSound);
        
        FormatEx (sDownload, sizeof (sDownload), "sound/%s", sSound);
        AddFileToDownloadsTable (sDownload);
    }

    if (g_hDatabase == null)
    {
        SQL_TConnect (OnSQLConnect, "default");
    }

    SetConVarFloat  (FindConVar ("mp_round_restart_delay"), 15.0);
    SetConVarInt    (FindConVar ("mp_timelimit"), 25);
    SetConVarInt    (FindConVar ("mp_maxrounds"), 10);

    ParseAdvertisements ("configs/goldc/advertisements.cfg");
    ParseSkinsConfig    ("configs/goldc/skins.cfg");
    ParseWeaponChanges  ("configs/goldc/weaponchanges.cfg");
}

public void OnClientPutInServer (int client)
{
    SDKHook (client, SDKHook_OnTakeDamagePost,  OnTakeDamagePost);
    SDKHook (client, SDKHook_GetMaxHealth,      OnMaxHealth);
    SDKHook (client, SDKHook_GetMaxHealth,      OnItemMaxHealth);
    SDKHook (client, SDKHook_WeaponSwitchPost,  Hook_WeaponSwitchPost);
    SDKHook (client, SDKHook_OnTakeDamage,      Hook_OnTakeDamage);
}

public void OnClientDisconnect (int client)
{
    g_iLastAction [client] = 0;
    g_iMoveTime   [client] = 0;
}

public void OnPluginEnd ()
{
    g_hTimer_Roundtimer = null;

    ClearSyncHudAll (g_hSync_RoundTimer);
    ClearSyncHudAll (g_hSync_PerkPoints);
    ClearSyncHudAll (g_hSync_CenterMessage);
    ClearSyncHudAll (g_hSync_CenterMessage2);
    ClearSyncHudAll (g_hSync_ZombiesRemaining);
    ClearSyncHudAll (g_hSync_SurvivorsRemaining);
}

public void OnMapEnd ()
{
    g_hTimer_Preparation = null;
    ResetVariables   ();
}