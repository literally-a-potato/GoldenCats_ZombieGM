// GInteraction.Chat Globals
Handle g_hArray_Adverts;
Handle g_hCookie_CenterDeathMessages;

int g_iCurrentAd;


// GInteraction.Classlimits Globals
ConVar convar_Limit_Scout;
ConVar convar_Limit_Soldier;
ConVar convar_Limit_Pyro;
ConVar convar_Limit_Demoman;
ConVar convar_Limit_Heavy;
ConVar convar_Limit_Engineer;
ConVar convar_Limit_Medic;
ConVar convar_Limit_Sniper;
ConVar convar_Limit_Spy;


// GInteraction.Healthscale Globals
int g_iLastAction [MAXPLAYERS + 1];
int g_iMaxHealth  [MAXPLAYERS + 1];


// GInteraction.Perks Globals
ConVar convar_PointsPerKill;
ConVar convar_HealthRegenTimer;
ConVar convar_HealthRegenCost;
ConVar convar_SpeedCost;
ConVar convar_CritsCost;

int g_iPerkPoints     [MAXPLAYERS + 1];
int g_iMedikitsUsed [MAXPLAYERS + 1];

bool g_bRegenerateHealth [MAXPLAYERS + 1];
bool g_bSpeedBuff          [MAXPLAYERS + 1];
bool g_bAlwaysCrit          [MAXPLAYERS + 1];

Handle g_hRegenTimer;
Handle g_hSync_PerkPoints;


// GInteraction.Skins Globals
ArrayList g_hArray_Skin_Names_Humans;
StringMap g_hTrie_Skin_Model_Humans;
StringMap g_hTrie_Skin_Class_Humans;

ArrayList g_hArray_Skin_Names_Zombies;
StringMap g_hTrie_Skin_Model_Zombies;
StringMap g_hTrie_Skin_Class_Zombies;


// GInteraction.Sounds Globals
ConVar convar_Sound_RoundEnd;
ConVar convar_Sound_RoundStart;
ConVar convar_Sound_HumanDies;
ConVar convar_Sound_LastManStanding;
ConVar convar_Sound_ZombiesWin;
ConVar convar_Sound_HumansWin;


// GInteraction.RoundTimer Globals
float  g_iTime;
Handle g_hTimer_Roundtimer;
Handle g_hSync_RoundTimer;


// GInteraction.WeaponChanges Globals
StringMap g_hTrie_WeaponChanges_Primary;
StringMap g_hTrie_WeaponChanges_Secondary;
StringMap g_hTrie_WeaponChanges_Melee;


// GInteraction.ZombieBase Globals
ConVar convar_Timer_Preparation;
ConVar convar_Timer_ZombieChoose;
ConVar convar_Timer_AFK;
ConVar convar_Timer_ZombieRespawn;

Handle g_hForward_OnInfected;

Handle g_hTimer_Preparation;
Handle g_hTimer_ZombieChoose;

int g_iMoveTime[MAXPLAYERS + 1];

Handle g_hSync_CenterMessage;
Handle g_hSync_CenterMessage2;
Handle g_hSync_ZombiesRemaining;
Handle g_hSync_SurvivorsRemaining;


// GGameserver.Statistics Globals
ConVar convar_ForumURL;
ConVar convar_Points_Kill_Gain_Survivors;
ConVar convar_Points_Kill_Gain_Zombies;
ConVar convar_Points_Death_Lose_Survivors;
ConVar convar_Points_Death_Lose_Zombies;

Handle g_hDatabase;

int g_iStat_TotalPoints[MAXPLAYERS + 1];
int g_iStat_TotalKills[MAXPLAYERS + 1];
int g_iStat_SurvivorKills[MAXPLAYERS + 1];
int g_iStat_ZombieKills[MAXPLAYERS + 1];
int g_iStat_TotalDeaths[MAXPLAYERS + 1];
int g_iStat_SurvivorDeaths[MAXPLAYERS + 1];
int g_iStat_ZombieDeaths[MAXPLAYERS + 1];

//float g_fStat_TotalTimePlayed[MAXPLAYERS + 1];