public void Event_OnPlayerSpawnWeapons (Event event, const char[] name, bool dontBroadcast)
{
    int userid = GetEventInt (event, "userid");
    int client = GetClientOfUserId (userid);
    
    if (client == 0 || client > MaxClients || !IsPlayerAlive (client))
    {
        return;
    }
    
    int weapon = GetActiveWeapon (client);
    Hook_WeaponSwitchPost (client, weapon);
}

public void Hook_WeaponSwitchPost (int client, int weapon)
{
    if (!IsValidEntity (weapon))
    {
        return;
    }
    
    TFClassType class = TF2_GetPlayerClass (client);
    
    char sClass[32];
    TF2_GetClassName (class, sClass, sizeof (sClass), false);
    
    int slot = GetWeaponSlot (client, weapon);
    
    if (slot == -1)
    {
        return;
    }
    
    TF2_RemoveCondition (client, TFCond_Kritzkrieged);
    TF2_RemoveCondition (client, TFCond_CritCola);
    
    char sMode[32];
    switch (slot)
    {
        case 0: GetTrieString (g_hTrie_WeaponChanges_Primary, sClass, sMode, sizeof (sMode));
        case 1: GetTrieString (g_hTrie_WeaponChanges_Secondary, sClass, sMode, sizeof (sMode));
        case 2: GetTrieString (g_hTrie_WeaponChanges_Melee, sClass, sMode, sizeof (sMode));
    }
    
    if (StrEqual (sMode, "crits"))
    {
        TF2_AddCondition (client, TFCond_Kritzkrieged, TFCondDuration_Infinite, 0);
    }
    else if (StrEqual (sMode, "minicrits"))
    {
        TF2_AddCondition (client, TFCond_CritCola, TFCondDuration_Infinite, 0);
    }
    
    char sWeapon[32];
    GetEntityClassname (weapon, sWeapon, sizeof (sWeapon));
    
    int index = GetEntProp (weapon, Prop_Send, "m_iItemDefinitionIndex");
    
    if (StrEqual (sWeapon, "tf_weapon_flaregun") || StrEqual (sWeapon, "tf_weapon_grenadelauncher") || StrEqual (sWeapon, "tf_weapon_crossbow") || StrEqual (sWeapon, "tf_weapon_compound_bow"))
    {
        TF2_AddCondition (client, TFCond_Kritzkrieged, TFCondDuration_Infinite, 0);
    }
    
    if (StrEqual (sWeapon, "tf_weapon_drg_pomson"))
    {
        TF2_AddCondition (client, TFCond_CritCola, TFCondDuration_Infinite, 0);
    }
    
    if (GoldC_IsZombie (client) && StrEqual (sWeapon, "tf_weapon_fists"))
    {
        TF2_AddCondition (client, TFCond_Kritzkrieged, TFCondDuration_Infinite, 0);
    }
    
    if (index == 61 || index == 1006)
    {
        TF2_RemoveCondition (client, TFCond_Kritzkrieged);
        TF2_RemoveCondition (client, TFCond_CritCola);
    }
}

public Action Hook_OnTakeDamage (int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
    if (victim == 0 || victim > MaxClients || attacker == 0 || attacker > MaxClients)
    {
        return Plugin_Continue;
    }
    
    bool changed;
    int active = GetActiveWeapon (attacker);
    
    if (!IsValidEntity (active))
    {
        return Plugin_Continue;
    }
    
    char sWeapon[32];
    GetEntityClassname (weapon, sWeapon, sizeof (sWeapon));
    
    int index = GetEntProp (active, Prop_Send, "m_iItemDefinitionIndex");
    
    if (!GoldC_IsZombie (attacker) && damagecustom == TF_CUSTOM_BASEBALL)
    {
        damage = 1000.0;
        changed = true;
    }
    
    if (GoldC_IsZombie (attacker) && damagecustom == TF_CUSTOM_BLEEDING)
    {
        damage = 35.0;
        changed = true;
    }
    
    if (damagecustom == TF_CUSTOM_TAUNT_GRENADE && victim > MaxClients)
    {
        char sEntity[MAX_NAME_LENGTH];
        GetEntityClassname (victim, sEntity, sizeof (sEntity));
        
        if (StrContains (sEntity, "obj_sentrygun"))
        {
            damage = 1000.0;
            changed = true;
        }
    }
    
    if (index == 230)
    {
        TF2_AddCondition (victim, TFCond_Milked, 10.0, attacker);
        damage = 1.25 * damage;
        changed = true;
    }
    
    if (index == 58 || index == 1083)
    {
        TF2_AddCondition (victim, TFCond_Milked, 10.0, attacker);
    }
    
    if (GoldC_IsZombie (attacker) && StrEqual (sWeapon, "tf_weapon_knife"))    //TF_CUSTOM_BACKSTAB
    {
        damage = 1000.0;
        changed = true;
    }
    
    if (!GoldC_IsZombie (attacker) && victim > MaxClients && (damagecustom == TF_CUSTOM_TAUNT_HADOUKEN || damagecustom == TF_CUSTOM_TAUNT_HIGH_NOON || damagecustom == TF_CUSTOM_TAUNT_GRAND_SLAM || damagecustom == TF_CUSTOM_TAUNT_FENCING || damagecustom == TF_CUSTOM_TAUNT_ARROW_STAB || damagecustom == TF_CUSTOM_TAUNT_GRENADE || damagecustom == TF_CUSTOM_TAUNT_BARBARIAN_SWING || damagecustom == TF_CUSTOM_TAUNT_UBERSLICE || damagecustom == TF_CUSTOM_TAUNT_ENGINEER_SMASH || damagecustom == TF_CUSTOM_TAUNT_ENGINEER_ARM || damagecustom == TF_CUSTOM_TAUNT_ARMAGEDDON || damagecustom == TF_CUSTOM_TAUNT_ALLCLASS_GUITAR_RIFF))
    {
        damage = 1000.0;
        changed = true;
    }
    
    return changed ? Plugin_Changed : Plugin_Continue;
}

public int TF2Items_OnGiveNamedItem_Post (int client, char[] classname, int itemDefinitionIndex, int itemLevel, int itemQuality, int entityIndex)
{
    if (GoldC_IsZombie (client))
    {
        return;
    }
    
    bool bRemove;
    switch (itemDefinitionIndex)
    {
        case 1101, 60, 59: bRemove = true;
    }

    if (bRemove && IsValidEntity (entityIndex))
    {
        Handle trie = CreateTrie ();
        SetTrieValue (trie, "client", client);
        SetTrieValue (trie, "itemDefinitionIndex", itemDefinitionIndex);
        SetTrieValue (trie, "entityIndex", entityIndex);

        CreateTimer (0.01, Timer_ReplaceWeapon, trie);
    }
}

public Action Timer_ReplaceWeapon (Handle timer, any data)
{
    int entityIndex = -1;
    GetTrieValue (data, "entityIndex", entityIndex);

    int client = -1;
    GetTrieValue (data, "client", client);

    int iItemDefinitionIndex = -1;
    GetTrieValue (data, "itemDefinitionIndex", iItemDefinitionIndex);

    CloseHandle (data);

    if (IsValidEntity (entityIndex))
    {
        int iSlot = GetWeaponSlot (client, entityIndex);

        RemovePlayerItem (client, entityIndex);
        AcceptEntityInput (entityIndex, "Kill");

        GiveReplacementItem (client, iSlot);
    }
}

void GiveReplacementItem (int client, int iSlot)
{
    TFClassType tfclass = TF2_GetPlayerClass (client);

    char sWeaponClassName[128];
    if (GetDefaultWeaponForClass (tfclass, iSlot, sWeaponClassName, sizeof (sWeaponClassName)))
    {
        int iOverrideIDI = GetDefaultIDIForClass (tfclass, iSlot);

        Handle hItem = TF2Items_CreateItem (OVERRIDE_CLASSNAME | OVERRIDE_ITEM_DEF | OVERRIDE_ITEM_LEVEL | OVERRIDE_ITEM_QUALITY | OVERRIDE_ATTRIBUTES);
        TF2Items_SetClassname (hItem, sWeaponClassName);
        TF2Items_SetItemIndex (hItem, iOverrideIDI);
        TF2Items_SetLevel (hItem, 1);
        TF2Items_SetQuality (hItem, 6);
        TF2Items_SetNumAttributes (hItem, 0);

        int iWeapon = TF2Items_GiveNamedItem (client, hItem);
        CloseHandle (hItem);

        EquipPlayerWeapon (client, iWeapon);
        SetPlayerWeaponAmmo (client, iWeapon, 0, 0);
    }
}

stock bool GetDefaultWeaponForClass (TFClassType xClass, int iSlot, char[] sOutput, int maxlen) {
    switch (xClass) {
        case TFClass_Scout: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_scattergun"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_pistol_scout"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_bat"); return true; }
            }
        }
        case TFClass_Sniper: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_sniperrifle"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_smg"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_club"); return true; }
            }
        }
        case TFClass_Soldier: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_rocketlauncher"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_shotgun_soldier"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_shovel"); return true; }
            }
        }
        case TFClass_DemoMan: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_grenadelauncher"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_pipebomblauncher"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_bottle"); return true; }
            }
        }
        case TFClass_Medic: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_syringegun_medic"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_medigun"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_bonesaw"); return true; }
            }
        }
        case TFClass_Heavy: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_minigun"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_shotgun_hwg"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_fists"); return true; }
            }
        }
        case TFClass_Pyro: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_flamethrower"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_shotgun_pyro"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_fireaxe"); return true; }
            }
        }
        case TFClass_Spy: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_revolver"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_builder"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_knife"); return true; }
                case 4: { Format (sOutput, maxlen, "tf_weapon_invis"); return true; }
            }
        }
        case TFClass_Engineer: {
            switch (iSlot) {
                case 0: { Format (sOutput, maxlen, "tf_weapon_shotgun_primary"); return true; }
                case 1: { Format (sOutput, maxlen, "tf_weapon_pistol"); return true; }
                case 2: { Format (sOutput, maxlen, "tf_weapon_wrench"); return true; }
                case 3: { Format (sOutput, maxlen, "tf_weapon_pda_engineer_build"); return true; }
            }
        }
    }

    Format (sOutput, maxlen, "");
    return false;
}

stock int GetDefaultIDIForClass (TFClassType xClass, int iSlot)
{
    switch (xClass) {
        case TFClass_Scout: {
            switch (iSlot) {
                case 0: { return 13; }
                case 1: { return 23; }
                case 2: { return 0; }
            }
        }
        case TFClass_Sniper: {
            switch (iSlot) {
                case 0: { return 14; }
                case 1: { return 16; }
                case 2: { return 3; }
            }
        }
        case TFClass_Soldier: {
            switch (iSlot) {
                case 0: { return 18; }
                case 1: { return 10; }
                case 2: { return 6; }
            }
        }
        case TFClass_DemoMan: {
            switch (iSlot) {
                case 0: { return 19; }
                case 1: { return 20; }
                case 2: { return 1; }
            }
        }
        case TFClass_Medic: {
            switch (iSlot) {
                case 0: { return 17; }
                case 1: { return 29; }
                case 2: { return 8; }
            }
        }
        case TFClass_Heavy: {
            switch (iSlot) {
                case 0: { return 15; }
                case 1: { return 11; }
                case 2: { return 5; }
            }
        }
        case TFClass_Pyro: {
            switch (iSlot) {
                case 0: { return 21; }
                case 1: { return 12; }
                case 2: { return 2; }
            }
        }
        case TFClass_Spy: {
            switch (iSlot) {
                case 0: { return 24; }
                case 1: { return 735; }
                case 2: { return 4; }
                case 4: { return 30; }
            }
        }
        case TFClass_Engineer: {
            switch (iSlot) {
                case 0: { return 9; }
                case 1: { return 22; }
                case 2: { return 7; }
                case 3: { return 25; }
            }
        }
    }

    return -1;
}

int GetWeaponSlot (int client, int weapon)
{
    if (!IsValidEntity (weapon))
    {
        return -1;
    }
    
    for (int i = 0; i < 8; i++)
    {
        int slot_weapon = GetPlayerWeaponSlot (client, i);
        
        if (IsValidEntity (slot_weapon) && slot_weapon == weapon)
        {
            return i;
        }
    }
    
    return -1;
}

void ParseWeaponChanges (const char[] config)
{
    char sPath[PLATFORM_MAX_PATH];
    BuildPath (Path_SM, sPath, sizeof (sPath), config);
    
    KeyValues kv = CreateKeyValues ("goldc_weaponchanges");
    
    if (!FileToKeyValues (kv, sPath))
    {
        LogError ("Error finding weapons configuration file.");
        return;
    }
    
    if (KvJumpToKey (kv, "classes") && KvGotoFirstSubKey (kv))
    {
        ClearTrie (g_hTrie_WeaponChanges_Primary);
        ClearTrie (g_hTrie_WeaponChanges_Secondary);
        ClearTrie (g_hTrie_WeaponChanges_Melee);
        
        do
        {
            char sClass[32];
            KvGetSectionName (kv, sClass, sizeof (sClass));
            
            char sPrimary[32];
            KvGetString (kv, "primary", sPrimary, sizeof (sPrimary));
            SetTrieString (g_hTrie_WeaponChanges_Primary, sClass, sPrimary);
            
            char sSecondary[32];
            KvGetString (kv, "secondary", sSecondary, sizeof (sSecondary));
            SetTrieString (g_hTrie_WeaponChanges_Secondary, sClass, sSecondary);
            
            char sMelee[32];
            KvGetString (kv, "melee", sMelee, sizeof (sMelee));
            SetTrieString (g_hTrie_WeaponChanges_Melee, sClass, sMelee);
        }
        while (KvGotoNextKey (kv));
        
        KvRewind (kv);
    }
    
    CloseHandle (kv);
    LogMessage ("Successfully parsed weapon changes.");
}