


public void OnTakeDamagePost (int victim, int attacker, int inflictor, float damage, int damagetype, int weapon, const float damageForce[3], const float damagePosition[3], int damagecustom)
{
    g_iLastAction[victim] = GetTime ();
}

public Action OnMaxHealth (int entity, int &maxhealth)
{
    /*if (GoldC_IsZombie (entity))
    {
        if (TF2_GetPlayerClass (entity) == TFClass_Heavy)
        {
            g_iMaxHealth[entity] = 750;
        }
        else
        {
            g_iMaxHealth[entity] = 550;
        }
    }
    else
    {
        g_iMaxHealth[entity] = 400;
    }*/

    g_iMaxHealth[entity] = maxhealth;
    return Plugin_Changed;
}


public Action Timer_GiveHealth (Handle timer)
{
    int time = GetTime ();

    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame (i) && IsPlayerAlive (i) && time - g_iLastAction[i] > 2)
        {
            int give;

            if (GoldC_IsZombie (i))
            {
                give = 5;
            }
            else
            {
                give = 2;
            }

            int new_health = GetClientHealth (i) + give;

            if (new_health > g_iMaxHealth[i])
            {
                new_health = g_iMaxHealth[i];
            }

            SetEntityHealth (i, new_health);
        }
    }
}

float GetClientSpeed (int client)
{
    float vecVelocity[3];
    GetEntPropVector (client, Prop_Data, "m_vecVelocity", vecVelocity);

    return SquareRoot (Pow (vecVelocity[0], 2.0) + Pow (vecVelocity[1], 2.0));
}