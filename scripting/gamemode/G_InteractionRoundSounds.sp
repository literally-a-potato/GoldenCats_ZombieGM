public void Event_OnRoundWin (Event event, const char[] name, bool dontBroadcast)
{
    char sSound[PLATFORM_MAX_PATH];
    GetConVarString (convar_Sound_RoundEnd, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        EmitSoundToAll (sSound);
    }
    
    switch (GetEventInt (event, "team"))
    {
        case 2:
        {
            GetConVarString (convar_Sound_ZombiesWin, sSound, sizeof (sSound));
            
            if (strlen (sSound) > 0)
            {
                EmitSoundToAll (sSound);
            }
        }
        case 3:
        {
            GetConVarString (convar_Sound_HumansWin, sSound, sizeof (sSound));
            
            if (strlen (sSound) > 0)
            {
                EmitSoundToAll (sSound);
            }
        }
    }
}

public void Event_OnRoundStart (Event event, const char[] name, bool dontBroadcast)
{
    char sSound[PLATFORM_MAX_PATH];
    GetConVarString (convar_Sound_RoundStart, sSound, sizeof (sSound));
    
    if (strlen (sSound) > 0)
    {
        EmitSoundToAll (sSound);
    }
}

public void Event_OnPlayerSpawnSound (Event event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId (GetEventInt (event, "userid"));
    
    if (client == 0 || client > MaxClients)
    {
        return;
    }
    
    if (!GoldC_IsZombie (client))
    {
        char sSound[PLATFORM_MAX_PATH];
        GetConVarString (convar_Sound_HumanDies, sSound, sizeof (sSound));
        
        if (strlen (sSound) > 0)
        {
            EmitSoundToAll (sSound);
        }
    }
}