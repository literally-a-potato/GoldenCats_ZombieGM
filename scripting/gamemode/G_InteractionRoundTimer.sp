public void Event_OnRoundStartTimer (Event event, const char[] name, bool dontBroadcast)
{
    KillTimerSafe (g_hTimer_Roundtimer);
    g_hTimer_Roundtimer = CreateTimer (1.0, Timer_Countdown, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_Countdown (Handle timer)
{
    g_iTime--;
    
    SetHudTextParams (0.0, 0.0, 5.0, 255, 255, 255, 255);
    ShowSyncHudTextAll (g_hSync_RoundTimer, "Time: %i", g_iTime);
    
    if (g_iTime <= 0)
    {
        g_iTime = 0.0;
        g_hTimer_Roundtimer = null;
        
        return Plugin_Stop;
    }
    
    return Plugin_Continue;
}