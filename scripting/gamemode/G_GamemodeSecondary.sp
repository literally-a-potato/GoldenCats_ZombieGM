
public Action OnPlayerRunCmd (int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
    if (!GoldC_IsZombie (client) && GetClientSpeed (client) >= 1.0)
    {
        g_iLastAction[client] = GetTime ();
    }

    if (client == 0 || client > MaxClients || IsFakeClient (client) || GetConVarInt (convar_Timer_AFK) == 0)
    {
        return Plugin_Continue;
    }

    if (buttons & IN_FORWARD || buttons & IN_BACK || buttons & IN_LEFT || buttons & IN_RIGHT || buttons & IN_JUMP || buttons & IN_DUCK)
    {
        g_iMoveTime[client] = GetTime ();
    }

    return Plugin_Continue;
}

public void TF2_OnConditionRemoved (int client, TFCond condition)
{
    if (condition == TFCond_SpeedBuffAlly && g_bSpeedBuff[client])
    {
        TF2_AddCondition (client, TFCond_SpeedBuffAlly, TFCondDuration_Infinite);
    }
}

public Action TF2_CalcIsAttackCritical (int client, int weapon, char[] weaponname, bool &result)
{
    if (g_bAlwaysCrit[client])
    {
        result = true;
        return Plugin_Changed;
    }

    if (!GoldC_IsZombie (client) && IsValidEntity (weapon) && StrEqual (weaponname, "tf_weapon_rocketlauncher_directhit"))
    {
        result = false;
        return Plugin_Changed;
    }

    return Plugin_Continue;
}


public APLRes AskPluginLoad2 (Handle myself, bool late, char[] error, int err_max)
{
    RegPluginLibrary ("goldc_zombies");
    CreateNative ("GoldC_IsZombie", Native_IsZombie);
    g_hForward_OnInfected = CreateGlobalForward ("GoldC_OnInfected", ET_Ignore, Param_Cell);
    return APLRes_Success;
}