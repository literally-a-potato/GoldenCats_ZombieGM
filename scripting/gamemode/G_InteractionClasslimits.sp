public void Event_OnPlayerSpawn (Event event, const char[] name, bool dontBroadcast)
{
    int userid = GetEventInt         (event, "userid");
    int client = GetClientOfUserId     (userid);

    if (client == 0 || client > MaxClients || !IsClientInGame (client) || !IsPlayerAlive (client))
    {
        return;
    }

    TFClassType class     = TF2_GetPlayerClass (client);
    TFTeam team         = TF2_GetClientTeam  (client);

    if (!CanGoClass (class, team))
    {
        PrintToChat         (client, "The maximum amount of allowed slots for this class are filled.");
        TF2_SetPlayerClass     (client, TFClass_Scout, false, true);
        TF2_RespawnPlayer     (client);
    }
}

public Action Listern_OnJoinClass (int client, const char[] command, int args)
{
    TFClassType class     = TF2_GetPlayerClass (client);
    TFTeam team         = TF2_GetClientTeam  (client);

    if (!CanGoClass (class, team))
    {
        PrintToChat         (client, "The maximum amount of allowed slots for this class are filled.");
        TF2_SetPlayerClass     (client, TFClass_Scout, false, true);
        return Plugin_Handled;
    }

    return Plugin_Continue;
}

bool CanGoClass (TFClassType class, TFTeam team)
{
    int max;
    switch (class)
    {
        case TFClass_Scout:        max = GetConVarInt (convar_Limit_Scout);
        case TFClass_Soldier:    max = GetConVarInt (convar_Limit_Soldier);
        case TFClass_Pyro:        max = GetConVarInt (convar_Limit_Pyro);
        case TFClass_DemoMan:    max = GetConVarInt (convar_Limit_Demoman);
        case TFClass_Heavy:        max = GetConVarInt (convar_Limit_Heavy);
        case TFClass_Engineer:    max = GetConVarInt (convar_Limit_Engineer);
        case TFClass_Medic:        max = GetConVarInt (convar_Limit_Medic);
        case TFClass_Sniper:    max = GetConVarInt (convar_Limit_Sniper);
        case TFClass_Spy:        max = GetConVarInt (convar_Limit_Spy);
    }

    if (max != -1 && GetClassAmount (class, team) > max)
    {
        return false;
    }

    return true;
}

int GetClassAmount (TFClassType class, TFTeam team)
{
    int amount;
    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame (i) && TF2_GetPlayerClass (i) == class && TF2_GetClientTeam (i) == team)
        {
            amount++;
        }
    }

    return amount;
}
